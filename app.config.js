import 'dotenv/config';

export default ({
    config
}) => {
    return {
        ...config,
        extra: {
            authApi: process.env.AUTH_API,
            moviesApi: process.env.MOVIES_API,
            moviesApiKey: process.env.MOVIES_API_KEY,
        }
    };
};