module.exports = function(api) {
    api.cache(true);
    return {
        presets: ['babel-preset-expo'],
        plugins: [
            'inline-dotenv', 
            [
                'module-resolver', {
                    root: ['./'],
                    extensions: [
                        '.ios.ts',
                        '.android.ts',
                        '.ts',
                        '.ios.tsx',
                        '.android.tsx',
                        '.tsx',
                        '.jsx',
                        '.js',
                        '.json',
                    ],
                    alias: {
                        '@app': './src',
                        '@api': './src/api',
                        '@assets': './assets',
                        '@components': './src/components',
                        '@context': './src/context',
                        '@helpers': './src/helpers',
                        '@navigation': './src/navigation',
                        '@screens': './src/screens',
                        '@themes': './src/themes',
                    },
                },
            ],
            'react-native-reanimated/plugin'
        ],
        env: {
            production: {
                plugins: [
                    'react-native-paper/babel',
                ],
            },
        },
    };
};