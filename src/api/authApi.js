import Constants from 'expo-constants'
const {
    authApi
} = Constants.manifest.extra

export const loginRequest = async (email, password) => {
    try {
        const body = {
            email: email,
            password: password
        }
        const rawResponse = await fetch(`${authApi}/login`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(body)
        })

        const response = await rawResponse.json()
        return response
    } catch (error) {
        console.log(error)
    }
}