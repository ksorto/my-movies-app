import Constants from 'expo-constants'
const {
    moviesApi,
    moviesApiKey
} = Constants.manifest.extra

export const configuration = async () => {
    try {
        const jsonResponse = await fetch(`${moviesApi}/configuration?api_key=${moviesApiKey}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
        const response = await jsonResponse.json()
        return response
    } catch (error) {
        console.log(error)
    }
}

export const genres = async () => {
    try {
        const jsonResponse = await fetch(`${moviesApi}/genre/movie/list?api_key=${moviesApiKey}&language=en-US`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
        const response = await jsonResponse.json()
        return response
    } catch (error) {
        console.log(error)
    }
}

export const popular = async () => {
    try {
        const jsonResponse = await fetch(`${moviesApi}/movie/popular?api_key=${moviesApiKey}&language=en-US`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
        const response = await jsonResponse.json()
        return response
    } catch (error) {
        console.log(error)
    }
}

export const recommendations = async (id) => {
    try {
        const jsonResponse = await fetch(`${moviesApi}/movie/${id}/recommendations?api_key=${moviesApiKey}&language=en-US`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
        const response = await jsonResponse.json()
        return response
    } catch (error) {
        console.log(error)
    }
}

export const search = async (query) => {
    try {
        const jsonResponse = await fetch(`${moviesApi}/search/movie?api_key=${moviesApiKey}&language=en-US&query=${query}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
        const response = await jsonResponse.json()
        return response
    } catch (error) {
        console.log(error)
    }
}