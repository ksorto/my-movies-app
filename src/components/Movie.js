import React, {
    useState,
    useEffect
} from 'react'
import {
    Avatar,
    Caption,
    Paragraph,
    Text,
    Title,
    useTheme
} from 'react-native-paper'
import { BlurView } from 'expo-blur'
import {
    Image,
    View,
    TouchableOpacity
} from 'react-native'
import { useNavigation } from '@react-navigation/native'
import Numeral from "numeral"
import Moment from 'moment'

const Movie = ({movie, compact = false}) => {
    const {
        colors
    } = useTheme()
    const navigation = useNavigation()

    const movieItemClicked = () => {
        navigation.navigate('Details', {
            movie: movie
        })
    }

    if(compact) {
        return (
            <TouchableOpacity onPress={movieItemClicked} activeOpacity={0.9} accessibilityRole='button'>
                
                <View style={{flexDirection: 'column', marginRight: 15, width: 120}}>
                    <Image source={{uri: `https://image.tmdb.org/t/p/w185/${movie?.poster_path}`}} 
                        style={{
                            width: '100%', 
                            height: 120,
                            borderRadius: 10,
                            resizeMode: 'cover',
                            marginBottom: 5
                    }}/>
                    <View style={{flexDirection: 'column',}}>
                        <Text numberOfLines={2} style={{fontSize: 15, fontWeight: '700', marginBottom: 0}}>{movie.title}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <TouchableOpacity onPress={movieItemClicked} activeOpacity={0.9} accessibilityRole='button'>
            <BlurView  intensity={100} tint="dark" style={{
                marginVertical: 20,
                borderRadius: 10
            }}>
                <View style={{flexDirection: 'column', padding: 10}}>
                    <View style={{
                        flexDirection: 'row',
                    }}>
                        <Image source={{uri: `https://image.tmdb.org/t/p/w342/${movie?.poster_path}`}} 
                            style={{
                                width: 100, 
                                height: 130,
                                borderRadius: 10,
                                resizeMode: 'cover',
                                position: 'absolute',
                                top: -30
                            }
                        }/>
                        <View style={{width: 100}} />
                        <View style={{flex: 1, paddingLeft: 10}}>
                            <Title numberOfLines={1}>{movie.title}</Title>
                            <Caption numberOfLines={3} style={{fontSize: 14, textAlign: 'justify'}}>{movie.overview}</Caption>
                        </View>
                    </View>
                    <View style={{paddingTop: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Avatar.Icon 
                                size={32} 
                                style={{backgroundColor: 'transparent'}} 
                                color={colors.yellow} 
                                icon="star-outline" />
                            <Text style={{color: colors.yellow, fontWeight: '700', fontSize: 16}}>
                                {Numeral(movie.vote_average).format('0,0.[00]')}
                            </Text>
                        </View>
                        <Text style={{fontWeight: '700', color: colors.gray500}}>
                            {Moment(movie.release_date).format('MMMM DD')}
                        </Text>
                    </View>
                </View>
            </BlurView>
        </TouchableOpacity>
    )
}

export default Movie