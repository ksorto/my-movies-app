import 'react-native-gesture-handler'
import {
    createStackNavigator
} from '@react-navigation/stack'
import LoginScreen from '@screens/LoginScreen'

const AuthStack = createStackNavigator()

const AuthStackNavigator = () => {
    return (
        <AuthStack.Navigator 
            initialRouteName={"Login"}  
            screenOptions={
                {
                    headerShown: false
                }
            }
        >
            <AuthStack.Screen
              name="Login"
              component={LoginScreen}
            />
        </AuthStack.Navigator>
    )
}

export default AuthStackNavigator