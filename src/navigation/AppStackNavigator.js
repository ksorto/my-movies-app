import 'react-native-gesture-handler'
import {
    createStackNavigator
} from '@react-navigation/stack'
import HomeScreen from '@screens/HomeScreen'
import MovieDetailScreen from '@screens/MovieDetailScreen'
import MoviesSearchScreen from '@screens/MoviesSearchScreen'

const AppStack = createStackNavigator()

const AppStackNavigator = () => {
    return (
        <AppStack.Navigator 
            initialRouteName={"Home"}  
            screenOptions={
                {
                    headerShown: false,
                    safeAreaInsets: {
                        top: 0
                    }
                }
            }
        >
            <AppStack.Screen
              name="Home"
              component={HomeScreen}
            />
            <AppStack.Screen
              name="Details"
              component={MovieDetailScreen}
            />
            <AppStack.Screen
              name="Search"
              component={MoviesSearchScreen}
            />
        </AppStack.Navigator>
    )
}

export default AppStackNavigator