import {loginRequest} from '@api/authApi'
import AsyncStorage from '@react-native-async-storage/async-storage'

export const login = async (email, password) => {
    const response = await loginRequest(email, password)
    const { token } = response || {}

    if(!token) {
        return false
    }

    await AsyncStorage.setItem('@myMoviesApp:token', token)

    return true
}

export const logout = async () => {
    await AsyncStorage.removeItem('@myMoviesApp:token')
}