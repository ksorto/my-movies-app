import {
    DarkTheme as NavigationDarkTheme,
    DefaultTheme as NavigationDefaultTheme,
} from '@react-navigation/native';
import {
    DarkTheme as PaperDarkTheme,
    DefaultTheme as PaperDefaultTheme
} from 'react-native-paper';

export const CombinedDarkTheme = {
    ...PaperDarkTheme,
    ...NavigationDarkTheme,
    mode: 'adaptive',
    myOwnProperty: true,
    colors: {
        ...PaperDarkTheme.colors,
        ...NavigationDarkTheme.colors,
        background: '#040608',
        primary: '#FF6694',
        secondary: '#FFFEFD',
        white: '#ffffff',
        yellow: '#FFBF0F',
        gray300: "#E0E0E0",
        gray400: "#BDBDBD",
        gray500: "#9E9E9E",
    },
    roundness: 50
};