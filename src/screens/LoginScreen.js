import {
    useContext,
    useEffect,
    useState
} from 'react' 
import {
    Image,
    Keyboard,
    Platform,
    StyleSheet,
    View
} from 'react-native'
import {
    Button,
    Caption,
    Headline,
    HelperText,
    Text,
    TextInput,
    useTheme
} from 'react-native-paper'
import {
    login
} from '@helpers/authService'
import {
    AppContext
} from '@context/AppContext'
import _ from 'lodash'

const appLogo = require('@assets/icon.png')
const LoginScreen = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [invalidForm, setInvalidForm] = useState(false)
    const [invalidFormMessage, setInvalidFormMessage] = useState('')
    const [invalidAuth, setInvalidAuth] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
    const { colors } = useTheme()
    const {
        signIn
    } = useContext(AppContext)

    const isValidEmail = (value) => {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(value)
    }

    const signInButtonClick = async () => {
        if(isLoading) {
            return
        }

        setInvalidForm(false)
        setIsLoading(true)
        setInvalidFormMessage('')

        if(_.isEmpty(email) || _.isEmpty('password')) {
            setInvalidForm(true)
            setInvalidFormMessage('Email/Password required!')
            setIsLoading(false)
            return
        }

        if(!isValidEmail(email)) {
            setInvalidForm(true)
            setInvalidFormMessage('Invalid email format!')
            setIsLoading(false)
            return
        }
        const isAuth = await login(email, password)
        setIsLoading(false)
        setInvalidAuth(!isAuth)
        if(isAuth) {
            setEmail('')
            setPassword('')
        }
        signIn(isAuth)
    }
    return (
        <View style={styles.container}>
            <View style={styles.form}>
                <Image style={styles.appLogo} source={appLogo}/>
                <View>
                    <Headline style={{marginBottom: 0, fontSize: 32, textAlign: 'center'}}>
                        Welcome Back!
                    </Headline>
                    <Caption style={{fontSize: 16, textAlign: 'center', marginBottom: 15}}>
                        Sig in to get started.
                    </Caption>
                    <TextInput 
                        label="Email"
                        keyboardType="email-address"
                        theme={{ colors: { primary: colors.primary}}}
                        value={email}
                        mode="flat"
                        onChangeText={text => {
                            setEmail(text)
                        }}
                        right={<TextInput.Icon name="mail-outline" color={colors.text} size={20} />}
                    />
                    <HelperText type="error">
                        {invalidForm && invalidFormMessage}
                    </HelperText>
                    <TextInput 
                        label="Password"
                        keyboardType="default"
                        theme={{ colors: { primary: colors.primary}}}
                        value={password}
                        mode="flat" 
                        secureTextEntry={true}
                        onChangeText={text => {
                            setPassword(text)
                        }}
                        right={<TextInput.Icon name="lock-closed-outline" color={colors.text} size={20} />}
                    />
                    <HelperText type="error">
                        {invalidAuth && "User not found!"}
                    </HelperText>
                    <Button 
                        icon="arrow-forward-outline" 
                        contentStyle={{flexDirection: 'row-reverse', padding: 5}}
                        mode="contained" 
                        loading={isLoading}
                        style={{marginTop: 5, borderColor: colors.primary, borderRadius: 30}}
                        dark={true}
                        onPress={signInButtonClick}>
                        Sign In!
                    </Button>
                    <Caption style={{textAlign: 'center'}}>
                        By signing in you agree to our Terms of Service, Privacy Policy and Cookie Policy.
                    </Caption>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        flexDirection: 'column', 
        justifyContent: 'center',
        alignItems: 'center',
    },
    form: {
        width: '80%',
        flexDirection: 'column',
    },
    appLogo: {
        width: 200,
        height: 200,
        alignSelf: 'center',
        resizeMode: 'contain',
        marginBottom: 20
    }
})

export default LoginScreen