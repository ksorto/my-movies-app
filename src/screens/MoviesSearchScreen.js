import {
    useEffect,
    useState
} from 'react'
import {
    FlatList,
    Platform,
    SafeAreaView,
    StatusBar,
    View
} from 'react-native'
import {
    Appbar,
    Text,
    useTheme
} from 'react-native-paper'
import Movie from '@components/Movie'
import {
    search
} from '@api/moviesApi'
import { BlurView } from 'expo-blur'
import LottieView from 'lottie-react-native'

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 0 : StatusBar.currentHeight

const MoviesSearchScreen = ({route, navigation}) => {
    const query = route.params.query
    const {
        colors
    } = useTheme()
    const [isLoading, setIsLoading] = useState(true)
    const [movies, setMovies] = useState([])

    useEffect(() => {
        (async () => {
            const response = await search(query)
            const { results } = response || {}
            if(!results) {
                setIsLoading(false)
                return
            }
            setIsLoading(false)
            setMovies(results)
        })()
    }, [])

    const Content = () => {
        if (isLoading) {
            return (
                <View style={{padding: 15, alignItems: 'center', flex: 1, justifyContent: 'center'}}>
                    <LottieView
                        style={{
                        width: 200,
                        height: 200,
                        }}
                        source={require('@assets/loading.json')}
                        loop
                        autoPlay
                    />
                </View>
            )    
        }
        return <FlatList 
            contentContainerStyle={{paddingHorizontal: 15, paddingTop: 25}}
            data={movies}
            renderItem={({item}) => <Movie movie={item}/>}
            keyExtractor={item => item.id}
        />
    }

    return (
        <SafeAreaView style={{flex: 1}}>
            <BlurView intensity={10}>
                <Appbar.Header style={{backgroundColor: 'transparent'}} >
                    <Appbar.BackAction onPress={() => {navigation.goBack()}} color={colors.white}/>
                    <Appbar.Content title={query} color={colors.white}/>
                </Appbar.Header>
            </BlurView>
            <Content/>
        </SafeAreaView>
    )
}

export default MoviesSearchScreen