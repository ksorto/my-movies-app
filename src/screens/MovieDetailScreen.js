import React, {
    useEffect,
    useState
} from 'react'
import {
    FlatList,
    Image,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    View
} from 'react-native'
import {
    Chip,
    Headline,
    Paragraph,
    Text,
    Title,
    TouchableRipple,
    useTheme
} from 'react-native-paper'
import Icon from 'react-native-vector-icons/Ionicons'
import { LinearGradient } from 'expo-linear-gradient'
import {
    genres,
    recommendations
} from '@api/moviesApi'
import Movie from '@components/Movie'
import _ from 'lodash'
import Numeral from "numeral"
import Moment from 'moment'

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 0 : StatusBar.currentHeight
const GRADIENTS = ['rgba(0,0,0,0.6)', 'rgba(0,0,0,0.1)', 'transparent', 'transparent', 'transparent', 'rgba(0,0,0,0.2)']

const MovieDetailScreen = ({route, navigation}) => {
    const { movie } = route.params;
    const {
        colors
    } = useTheme()
    const [isLoading, setIsLoading] = useState(true)
    const [isLoadingGenres, setIsLoadingGenres] = useState(true)
    const [movies, setMovies] = useState([])
    const [genresList, setGenresList] = useState([])

    const loadRecommendations = async () => {
        const response = await recommendations(movie.id)
        const { results } = response || {}
        if(!results) {
            setIsLoading(false)
            return
        }
        setIsLoading(false)
        setMovies(results)
    }

    const loadGenres = async () => {
        const response = await genres()
        const { genres: results } = response || {}
        if(!results) {
            return
        }
        setGenresList(results)
        setIsLoadingGenres(false)
    }

    useEffect(() => {
        setIsLoading(true)
        loadGenres()
        loadRecommendations()
    }, [])

    useEffect(() => {
        loadRecommendations()
    }, [movie])

    const goBackButtonClicked = () => {
        navigation.goBack()
    }

    const getGenre = (id) => {
        const index = _.findIndex(genresList, g => g.id === id)
        return genresList[index] || {}
    }

    const Recommendations = () => {
        if(isLoading) {
            return <></>
        }
        return (
            <>
                <Title style={{marginVertical: 10}}>Recommendations</Title>
                <FlatList 
                    data={movies}
                    renderItem={({item}) => <Movie movie={item} compact={true}/>}
                    keyExtractor={item => item.id}
                    horizontal={true}
                />
            </>
        )
    }

    const Header = () => {
        return (
            <View style={{height: 320}}>
                <Image source={{uri: `https://image.tmdb.org/t/p/w1280/${movie?.backdrop_path}`}}
                    style={{
                        resizeMode: 'cover',
                        position: 'absolute',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                    }}
                />
                <LinearGradient colors={[
                        ...GRADIENTS,
                        colors.background
                    ]} 
                    style={{flex: 1, paddingTop: STATUSBAR_HEIGHT, flexDirection: 'column', justifyContent: 'space-between'}}>
                    <View style={{
                        flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'
                    }}>
                        <TouchableRipple rippleColor={"rgba(0, 0, 0, 0.0999)"}  style={{padding: 15}} onPress={goBackButtonClicked}>
                            <Icon name={'arrow-back-outline'} size={24} color={colors.white}/>
                        </TouchableRipple>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        paddingHorizontal: 15,
                        paddingVertical: 10
                    }}>
                        <Image 
                            source={{uri: `https://image.tmdb.org/t/p/w500/${movie?.poster_path}`}} 
                            style={{
                                width: 100, 
                                height: 130,
                                borderRadius: 10,
                                resizeMode: 'cover',
                                position: 'absolute',
                                bottom: -70,
                                left: 15
                            }}/>
                    </View>
                </LinearGradient>
            </View>
        )
    }

    return (
        <SafeAreaView style={{flex: 1}}>
            <ScrollView>  
                <View style={{flexDirection: 'column', flex: 1}}>
                    <Header />
                    <View style={{
                        paddingHorizontal: 15, 
                        paddingVertical: 10,
                    }}>
                        <View style={{
                            height: 80,
                            justifyContent: 'center',
                            paddingBottom: 15,
                            marginBottom: 0
                        }}>                    
                            <Title numberOfLines={2} style={{marginLeft: 115, fontSize: 24}}>
                                {movie.title}
                            </Title>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            flexWrap: "wrap",
                            paddingHorizontal: 5,
                            marginBottom: 10
                        }}>
                            {!isLoadingGenres && movie.genre_ids.map(id => (
                                <Chip 
                                    key={`genre-${id}`}
                                    style={{marginHorizontal: 5, marginBottom: 5}}>
                                    {getGenre(id)?.name}
                                </Chip>
                            ))}
                        </View>                   
                        <Paragraph style={{fontSize: 15, textAlign: 'justify'}}>
                            {movie.overview}
                        </Paragraph>
                        <View style={{flexDirection: 'row', paddingVertical: 20, alignItems: 'center'}}>
                            <Icon name={'star-outline'} size={24} color={colors.yellow}/>
                            <Text style={{fontSize: 16, fontWeight: '700', marginLeft: 5}}>
                                {Numeral(movie.vote_average).format('0,0.[00]')}/10
                            </Text>
                            <Text style={{
                                flex: 1, 
                                textAlign: 'right', 
                                fontSize: 16, 
                                fontWeight: '700',
                                color: colors.gray500
                            }}>
                                {Moment(movie.release_date).format('MMMM DD')}
                            </Text>
                        </View>
                        <Recommendations />
                    </View>
                </View>
             </ScrollView>
        </SafeAreaView>
    )
}

export default MovieDetailScreen