import {
    useContext,
    useEffect,
    useState
} from 'react' 
import {
    FlatList,
    Image,
    Platform,
    SafeAreaView,
    StatusBar,
    View
} from 'react-native'
import {
    Button,
    Dialog,
    Paragraph,
    Portal,
    Searchbar,
    Text,
    TouchableRipple,
    useTheme
} from 'react-native-paper'
import {
    AppContext
} from '@context/AppContext'
import {
    logout
} from '@helpers/authService'
import {
    popular
} from '@api/moviesApi'
import Movie from '@components/Movie'
import _ from 'lodash'
import Icon from 'react-native-vector-icons/Ionicons'
import LottieView from 'lottie-react-native'

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 0 : StatusBar.currentHeight

const HomeScreen = ({navigation}) => {
    const [searchQuery, setSearchQuery] = useState('')
    const [movies, setMovies] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [logoutDialogVisible, setLogoutDialogVisible] = useState(false)
    const {
        signOut
    } = useContext(AppContext)
    const {
        colors
    } = useTheme()

    const onChangeSearch = query => setSearchQuery(query)
    const hideLogoutDialog = () => setLogoutDialogVisible(false)
    const showLogoutDialog = () => setLogoutDialogVisible(true)

    useEffect(() => {
        (async () => {
            const response = await popular()
            const { results } = response || {}
            if(!results) {
                setIsLoading(false)
                return
            }
            setIsLoading(false)
            setMovies(results)
        })()
    }, [])

    const signOutButtonClick = async () => {
        await logout()
        signOut()
    }

    const searchBarOnIconPress = () => {
        if(_.isEmpty(searchQuery)) {
            return
        }
        navigation.navigate('Search', {
            query: searchQuery
        })
    }

    const Content = () => {
        if (isLoading) {
            return (
                <View style={{padding: 15, alignItems: 'center', flex: 1, justifyContent: 'center'}}>
                    <LottieView
                        style={{
                        width: 200,
                        height: 200,
                        }}
                        source={require('@assets/loading.json')}
                        loop
                        autoPlay
                    />
                </View>
            )    
        }
        return <FlatList 
            contentContainerStyle={{paddingHorizontal: 15, paddingTop: 20}}
            data={movies}
            renderItem={({item}) => <Movie movie={item}/>}
            keyExtractor={item => item.id}
        />
    }

    return (
        <SafeAreaView style={{flex: 1, backgroundColor: '#040608', paddingTop: STATUSBAR_HEIGHT}}>
            <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={{
                    flexDirection: 'row', 
                    justifyContent: 
                    'space-between', 
                    alignItems: 'center',
                    padding: 15
                }}>
                    <Text style={
                        {
                            color: colors.white, 
                            fontSize: 40, 
                            marginVertical: 10
                        }
                    }>
                        Popular Movies
                    </Text>
                    <TouchableRipple 
                        rippleColor={"rgba(255, 255, 255, 0.0999)"}  
                        style={{padding: 15}} 
                        onPress={showLogoutDialog}>
                        <Icon name={'log-out-outline'} size={24} color={colors.white}/>
                    </TouchableRipple>
                </View>
                <View style={{flexDirection: 'row', marginVertical: 5, paddingHorizontal: 15}}>
                    <Searchbar
                        placeholder="Search movies"
                        onChangeText={onChangeSearch}
                        onIconPress={searchBarOnIconPress}
                        onSubmitEditing={searchBarOnIconPress}
                        style={{flex:1}}
                        value={searchQuery}
                    />
                </View>
                <Content/>
            </View>
            <Portal>
                <Dialog 
                    visible={logoutDialogVisible} 
                    onDismiss={hideLogoutDialog} 
                    style={{borderRadius: 20}}>
                    <Dialog.Title>Are you leaving?</Dialog.Title>
                    <Dialog.Content>
                        <Paragraph>Are you sure want to log out? You'll be returned to the login screen.</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={hideLogoutDialog}>Cancel</Button>
                        <Button 
                            contentStyle={{flexDirection: 'row-reverse', padding: 5}}
                            onPress={signOutButtonClick} 
                            icon="arrow-forward-outline">
                            Yes
                        </Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </SafeAreaView>
    )
}


export default HomeScreen