import React, {
    createContext
} from 'react';

const initialState = {
    signIn: (isAuth) => {},
    signOut: () => {},
}

export const AppContext = createContext(initialState);