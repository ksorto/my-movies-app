# My Movies App
## Prerequisites
- [Node.js > 12](https://nodejs.org) and npm (Recommended: Use [nvm](https://github.com/nvm-sh/nvm)) or [Yarn](https://yarnpkg.com/)
- [Expo CLI](https://docs.expo.dev/workflow/expo-cli/)
- [Expo Go](https://expo.dev/client)

## Features

- See most popular movies
- Search movies
- See movie details
- See movie recommendations
- Login system

## Run

### Using scripts from console
Before to clone the repository, run the following scripts:
Install Dependencies
`yarn install` or `npm install`
Run the app using Expo
`expo start` or `yarn start`
Open the expo go app on your device, and scan the QR code to execute the app
If you are using Android, you can install manually the application, downloading the .apk file from [here](https://exp-shell-app-assets.s3.us-west-1.amazonaws.com/android/%40ksorto/my-movies-app-5437204f7ab14cc488f991be6133121f-signed.apk)

## Screenshots
![Screenshot_20220130-232733](/uploads/a91f974318af3d4a769150a483c52b84/Screenshot_20220130-232733.png)

![Screenshot_20220130-232716](/uploads/88959e884e175b1c6fe4afde8b83c4d0/Screenshot_20220130-232716.png)

![Screenshot_20220130-232654](/uploads/a72e35dd71e01e6c11a841c0e961097f/Screenshot_20220130-232654.png)

![Screenshot_20220130-232706](/uploads/349c111b9cc2e6df8ec31d60d078a907/Screenshot_20220130-232706.png)