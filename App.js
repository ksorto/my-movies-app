import 'react-native-gesture-handler'
import React, {
    useCallback,
    useEffect,
    useMemo,
    useState
} from 'react'
import {
    StatusBar
} from 'expo-status-bar'
import {
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    StyleSheet,
    View
} from 'react-native'
import {
    AppContext
} from '@context/AppContext'
import {
    ActivityIndicator,
    Provider as PaperProvider,
    Title
} from 'react-native-paper'
import Ionicons from 'react-native-vector-icons/Ionicons'
import {
    CombinedDarkTheme
} from '@themes/default'
import {
    NavigationContainer
} from '@react-navigation/native'
import AuthStackNavigator from '@navigation/AuthStackNavigator'
import AppStackNavigator from '@navigation/AppStackNavigator'
import AsyncStorage from '@react-native-async-storage/async-storage'
import * as SplashScreen from 'expo-splash-screen'

const App = () => {
    const [authenticated, setAuthenticated] = useState(false)
    const [appIsReady, setAppIsReady] = useState(false)
    let theme = CombinedDarkTheme

    const verifySession = async () => {
        const token = await AsyncStorage.getItem('@myMoviesApp:token')
        if(!token) {
            setAuthenticated(false)
            return
        }
        setAuthenticated(true)
    }

    useEffect(() => {
        (async () => {
            try {
                await SplashScreen.preventAutoHideAsync()
                await verifySession()
            } catch (error) {
                console.warn(e)
            } finally {
                setAppIsReady(true)
                await SplashScreen.hideAsync()
            }
        })()
    }, [])

    const signIn = useCallback((isAuth) => {
        setAuthenticated(isAuth)
    })

    const signOut = useCallback(() => {
        setAuthenticated(false)
    })

    const preferences = useMemo(() => ({
        signIn,
        signOut,
    }), [
        signIn,
        signOut,
    ])

    return (
        <View style={styles.container}>
            <AppContext.Provider value={preferences}>
                <PaperProvider
                    settings={{
                        icon: props => <Ionicons {...props} />,
                    }} theme={theme}> 
                    <SafeAreaView style={{flex: 1}}>
                        <StatusBar style="light" />
                        <KeyboardAvoidingView
                            behavior={Platform.OS === "ios" ? "padding" : "height"}
                            style={{flex: 1}}
                        >
                            {/*Add navigation*/}
                            <NavigationContainer theme={theme}>
                                {(authenticated ? <AppStackNavigator /> : <AuthStackNavigator/>)} 
                            </NavigationContainer>  
                        </KeyboardAvoidingView>
                    </SafeAreaView>
                </PaperProvider>                
            </AppContext.Provider>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

export default App;